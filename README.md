# LnaSystems

Tested with Elixir 1.9.1 (compiled with Erlang/OTP 22)

To create the executable file run
```
mix escript.build
```

Run the executable with the example input

```
./lna_systems files/input.txt
```


Or simply run the `build.sh` script. It will build and run the project with the example output.


# Tests

Run tests with

```
mix test
```

Or with the `test.sh` script to build and test the project inside a docker container.