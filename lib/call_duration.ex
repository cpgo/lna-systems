defmodule CallDuration do
  defstruct [:time_of_start, :time_of_finish, :call_from, :call_to, :call_time, :call_cost]
  @initial_pricing 5
  @normal_pricing 2
  @initial_pricing_timer 5
  def open_file(file) do
    lines = String.trim(file) |> String.split("\n") |> Enum.map(fn x -> String.split(x, ";") end)
    parse_file(lines)
  end

  def parse_file(lines) do
    Enum.map(lines, fn line -> parse_line(line) end)
  end

  def parse_line([time_of_start, time_of_finish, call_from, call_to]) do
    call_duration = duration_in_minutes(time_of_start, time_of_finish)

    %CallDuration{
      time_of_start: time_of_start,
      time_of_finish: time_of_finish,
      call_time: call_duration,
      call_cost: call_cost(call_duration),
      call_from: call_from,
      call_to: call_to
    }
  end

  def duration_in_minutes(time_of_start, time_of_finish) do
    {:ok, parsed_start} = Time.from_iso8601(time_of_start)
    {:ok, parsed_finish} = Time.from_iso8601(time_of_finish)
    Time.diff(parsed_finish, parsed_start) / 60
  end

  def call_cost(call_time) when call_time <= @initial_pricing_timer do
    call_time * @initial_pricing
  end

  def call_cost(call_time) do
    discounted_duration = call_time - @initial_pricing_timer
    discounted_price = discounted_duration * @normal_pricing
    initial_price = @initial_pricing_timer * @initial_pricing
    initial_price + discounted_price
  end

  def sum_caller_times(call) do
    call
    |> List.flatten()
    |> Enum.reduce(0, fn call, state -> call.call_time + state end)
  end

  def caller_with_longest_calls(calls) do
    {high_caller, _total} = Enum.max_by(calls, fn {_caller, value} -> value end)
    high_caller
  end

  def total(file) do
    calls = open_file(file)
    grouped_calls = Enum.group_by(calls, fn x -> x.call_from end)

    longest_caller =
      grouped_calls
      |> Enum.map(fn {caller, values} -> {caller, sum_caller_times(values)} end)
      |> caller_with_longest_calls()

    longest_call =
      grouped_calls
      |> Map.get(longest_caller)
      |> Enum.max_by(fn x -> x.call_time end)

    Enum.reject(calls, fn x -> x == longest_call end)
    |> Enum.reduce(0, fn call, state -> call.call_time + state end)
    |> Float.round
  end
end
