#!/bin/sh
set -eo pipefail

docker build . -t lna-builder && docker run -u 1000 -t -v $(pwd):/app lna-builder mix test