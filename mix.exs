defmodule LnaSystems.MixProject do
  use Mix.Project

  def project do
    [
      app: :lna_systems,
      version: "0.1.0",
      elixir: "~> 1.9-rc",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      escript: [main_module: LnaSystems.CLI]
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
    ]
  end
end
