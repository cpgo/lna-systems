#!/bin/sh
set -eo pipefail

docker build . -t lna-builder && docker run -u 1000 -v $(pwd):/app lna-builder mix escript.build && ./lna_systems files/input.txt