defmodule LnaSystemsTest do
  use ExUnit.Case

  @file_contents "
09:11:30;09:15:22;+351914374373;+351215355312
15:20:04;15:23:49;+351217538222;+351214434422
16:43:02;16:50:20;+351217235554;+351329932233
17:44:04;17:49:30;+351914374373;+351963433432
"

  test "Parse lines into structs" do
    assert CallDuration.open_file(@file_contents) == [
      %CallDuration{
        call_cost: 19.333333333333332,
        call_from: "+351914374373",
        call_time: 3.8666666666666667,
        call_to: "+351215355312",
        time_of_finish: "09:15:22",
        time_of_start: "09:11:30"
      },
      %CallDuration{
        call_cost: 18.75,
        call_from: "+351217538222",
        call_time: 3.75,
        call_to: "+351214434422",
        time_of_finish: "15:23:49",
        time_of_start: "15:20:04"
      },
      %CallDuration{
        call_cost: 29.6,
        call_from: "+351217235554",
        call_time: 7.3,
        call_to: "+351329932233",
        time_of_finish: "16:50:20",
        time_of_start: "16:43:02"
      },
      %CallDuration{
        call_cost: 25.866666666666667,
        call_from: "+351914374373",
        call_time: 5.433333333333334,
        call_to: "+351963433432",
        time_of_finish: "17:49:30",
        time_of_start: "17:44:04"
      }
    ]
  end

  test "Correct call duration" do
    assert CallDuration.duration_in_minutes("09:00:00", "09:10:00") == 10
    assert CallDuration.duration_in_minutes("09:00:00", "09:00:30") == 0.5
  end

  test "Call cost is right when duration is 5 minutes or less" do
    assert CallDuration.call_cost(5) == 25
    assert CallDuration.call_cost(1) == 5
    assert CallDuration.call_cost(2.5) == 12.5
  end

  test "Call cost is right when duration is greater than 5 minutes" do
    assert CallDuration.call_cost(6) == 27
    assert CallDuration.call_cost(8.5) == 32
    assert CallDuration.call_cost(8) == 31
  end

  test "Calculate the right value" do
    assert CallDuration.total(@file_contents) == 15.0
  end
end
